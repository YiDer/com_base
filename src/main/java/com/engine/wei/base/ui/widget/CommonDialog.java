package com.engine.wei.base.ui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatDialog;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import com.engine.wei.base.R;
import com.engine.wei.base.databinding.DialogCommonBinding;


/**
 * 作者:赵若位
 * 时间:2021/3/4 21:38
 * 功能:
 */
public class CommonDialog extends AppCompatDialog implements LifecycleObserver, View.OnClickListener {

    private Context mContext;
    private Resources mResources;
    private String mTitle, mMessage, mLeft, mRight;
    private @ColorInt
    int mLeftTextColor, mRightTextColor;
    private OnCancelClickListener mOnCancelClickListener;
    private OnDeterminClickListener mOnDeterminClickListener;
    private boolean isLeftBold, isRightBold;
    private DialogCommonBinding mBinding;


    private CommonDialog(Builder builder) {
        super(builder.mContext, R.style.DialogCommonStyle);
        this.mContext = builder.mContext;
        mResources = mContext.getResources();
        this.mTitle = builder.mTitle;
        this.mMessage = builder.mMessage;
        this.mLeft = TextUtils.isEmpty(builder.mLeft) ? mResources.getString(R.string.cancel) : builder.mLeft;
        this.mRight = TextUtils.isEmpty(builder.mRight) ? mResources.getString(R.string.determine) : builder.mRight;
        this.mLeftTextColor = mResources.getColor(builder.mLeftTextColor);
        this.mRightTextColor = mResources.getColor(builder.mRightTextColor);
        this.mOnCancelClickListener = builder.mOnCancelClickListener;
        this.mOnDeterminClickListener = builder.mOnDeterminClickListener;
        this.isLeftBold = builder.isLeftBold;
        this.isRightBold = builder.isRightBold;
        initView();
    }

    private void initView() {
        mBinding = DialogCommonBinding.inflate(LayoutInflater.from(mContext));
        setContentView(mBinding.getRoot());
        mBinding.tvTitle.setText(mTitle);
        mBinding.tvTitle.setVisibility(TextUtils.isEmpty(mTitle) ? View.GONE : View.VISIBLE);
        mBinding.tvMessage.setText(mMessage);

        if (!TextUtils.isEmpty(mLeft)) {
            mBinding.tvLeft.setText(mLeft);
            mBinding.tvLeft.setTextColor(mLeftTextColor);
            mBinding.tvLeft.setOnClickListener(this);
            mBinding.tvLeft.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvLeft.setVisibility(View.GONE);
        }

        mBinding.tvLeft.setTypeface(Typeface.defaultFromStyle(isLeftBold ? Typeface.BOLD : Typeface.NORMAL));

        if (!TextUtils.isEmpty(mRight)) {
            mBinding.tvRight.setText(mRight);
            mBinding.tvRight.setTextColor(mRightTextColor);
            mBinding.tvRight.setOnClickListener(this);
            mBinding.tvRight.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvRight.setVisibility(View.GONE);
        }

        mBinding.tvRight.setTypeface(Typeface.defaultFromStyle(isRightBold ? Typeface.BOLD : Typeface.NORMAL));

        if (mContext instanceof LifecycleOwner) {
            ((LifecycleOwner) mContext).getLifecycle().addObserver(this);
        }
    }

    public void setOnCancelClickListener(OnCancelClickListener listener) {
        mOnCancelClickListener = listener;
    }

    public void setOnDeterminClickListener(OnDeterminClickListener listener) {
        mOnDeterminClickListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (v == mBinding.tvLeft) {
            if (null != mOnCancelClickListener) {
                mOnCancelClickListener.onClick(v);
            }
            dismiss();
            return;
        }

        if (v == mBinding.tvRight) {
            if (null != mOnDeterminClickListener) {
                mOnDeterminClickListener.onClick(v);
            }
            dismiss();
            return;
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy(LifecycleOwner owner) {
        dismiss();
        if (null != owner) {
            owner.getLifecycle().removeObserver(this);
        }
    }


    public static class Builder {
        private Context mContext;
        private String mTitle, mMessage, mLeft, mRight;
        private @ColorRes
        int mLeftTextColor, mRightTextColor;
        private OnCancelClickListener mOnCancelClickListener;
        private OnDeterminClickListener mOnDeterminClickListener;
        private boolean isLeftBold, isRightBold;


        public Builder(@Nullable Context context) {
            this.mContext = context;
            this.mLeft = context.getResources().getString(R.string.cancel);
            this.mRight = context.getResources().getString(R.string.determine);
            this.mLeftTextColor = R.color.clr_999999;
            this.mRightTextColor = R.color.clr_ff4444;
        }

        public Builder setTitle(String title) {
            this.mTitle = title;
            return this;
        }

        public Builder setTitle(@StringRes int stringRes) {
            this.mTitle = mContext.getResources().getString(stringRes);
            return this;
        }

        public Builder setMessage(@Nullable String message) {
            this.mMessage = message;
            return this;
        }

        public Builder setMessage(@StringRes int stringRes) {
            this.mMessage = mContext.getResources().getString(stringRes);
            return this;
        }

        public Builder setLeftText(String text) {
            this.mLeft = text;
            return this;
        }

        public Builder setLeftText(@StringRes int stringRes) {
            this.mLeft = mContext.getResources().getString(stringRes);
            return this;
        }

        public Builder setRightText(String text) {
            this.mRight = text;
            return this;
        }

        public Builder setRightText(@StringRes int stringRes) {
            this.mRight = mContext.getResources().getString(stringRes);
            return this;
        }

        public Builder setLeftTextColor(@ColorRes int textColor) {
            this.mLeftTextColor = textColor;
            return this;
        }

        public Builder setRightTextColor(@ColorRes int textColor) {
            this.mRightTextColor = textColor;
            return this;
        }


        public Builder setCancelClickListener(@Nullable OnCancelClickListener listener) {
            this.mOnCancelClickListener = listener;
            return this;
        }

        public Builder setDeterminClickListener(@Nullable OnDeterminClickListener listener) {
            this.mOnDeterminClickListener = listener;
            return this;
        }

        public Builder setLeftTextBold(boolean isBold) {
            this.isLeftBold = isBold;
            return this;
        }

        public Builder setRightTextBold(boolean isBold) {
            this.isRightBold = isBold;
            return this;
        }

        public CommonDialog createNormalDialog(@Nullable String message, OnCancelClickListener left, OnDeterminClickListener right) {
            this.mMessage = message;
            this.mOnCancelClickListener = left;
            this.mOnDeterminClickListener = right;
            return new CommonDialog(this);
        }

        public CommonDialog create() {
            return new CommonDialog(this);
        }
    }


    public interface OnCancelClickListener {
        void onClick(View view);
    }

    public interface OnDeterminClickListener {
        void onClick(View view);
    }
}
