package com.engine.wei.base.ui.imageLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * 作者:赵若位
 * 时间:2020/9/28 17:23
 * 功能:
 */
@Singleton
public final class ImageLoader implements IImageLoader {

    @Inject
    Context mContext;

    @Inject
    public ImageLoader() {

    }

    /*暂停Glide加载*/
    public void pause(@Nullable Context context) {
        GlideApp.with(context)
                .pauseRequestsRecursive();
    }

    /*继续Glide加载*/
    public void resume(@Nullable Context context) {
        GlideApp.with(context)
                .resumeRequestsRecursive();
    }

    /*清除硬盘缓存*/
    public static void clearDiskCache(@NonNull Context context) {
        GlideApp.get(context)
                .clearDiskCache();
    }

    /*清除内存缓存*/
    public static void clearMemory(@NonNull Context context) {
        Glide.get(context)
                .clearMemory();
    }

    public static void onTrimMemory(@NonNull Context context, int level) {
        GlideApp.get(context)
                .onTrimMemory(level);
    }


    public static void onLowMemory(@NonNull Context context) {
        GlideApp.get(context)
                .onLowMemory();
    }

    protected GlideRequest<Bitmap> createRequest(Context context) {
        return GlideApp.with(null == context ? mContext : context)
                .asBitmap()
                .centerCrop();
    }

    @Override
    public void loadImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView) {
        createRequest(context)
                .load(object)
                .override(imageView.getWidth(), imageView.getHeight())
                .into(imageView);
    }

    @Override
    public void loadImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView, RequestOptions options) {
        createRequest(context)
                .load(object)
                .apply(options)
                .override(imageView.getWidth(), imageView.getHeight())
                .into(imageView);

    }

    @Override
    public void loadCircleImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView) {
        createRequest(context)
                .load(object)
                .circleCrop()
                .override(imageView.getWidth(), imageView.getHeight())
                .into(imageView);
    }

    @Override
    public void loadCircleImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView, RequestOptions options) {
        createRequest(context)
                .load(object)
                .circleCrop()
                .apply(options)
                .override(imageView.getWidth(), imageView.getHeight())
                .into(imageView);
    }

    @Override
    public void loadImageByOptions(@Nullable Context context, @Nullable Object obj, @Nullable ImageView imageView, RequestOptions options) {
        createRequest(context)
                .load(obj)
                .apply(options)
                .override(imageView.getWidth(), imageView.getHeight())
                .into(imageView);
    }

}
