package com.engine.wei.base.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.SparseArray;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 作者:赵若位
 * 时间:2021/8/25 16:49
 * 功能:
 */
public class BaseViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;
    private Resources mResources;
    private static SparseArray<View> sViews;


    private BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        if (null == itemView) {
            throw new NullPointerException("The itemView of BaseViewHolder is empty!");
        }
        this.mContext = itemView.getContext();
        mResources = mContext.getApplicationContext().getResources();
        sViews = new SparseArray<>();
    }

    public static BaseViewHolder createViewHolder(@NonNull View itemView) {
        return new BaseViewHolder(itemView);
    }

    public <V extends View> V getView(@IdRes int res) {
        if (null != sViews) {
            V view;
            if (sViews.indexOfKey(res) < 0 || null == (view = (V) sViews.get(res))) {
                view = itemView.findViewById(res);
                sViews.put(res, view);
            }
            return view;
        }
        return null;
    }

}
