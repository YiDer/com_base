package com.engine.wei.base.ui.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.engine.wei.base.mvp.contract.BaseContract;
import com.engine.wei.base.mvp.presenter.BasePresenter;
import com.engine.wei.base.ui.widget.ToastManager;
import com.engine.wei.base.utils.log.LogUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.trello.rxlifecycle4.LifecycleTransformer;
import com.trello.rxlifecycle4.android.ActivityEvent;
import com.trello.rxlifecycle4.components.support.RxAppCompatActivity;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 作者:赵若位
 * 时间:2021/8/25 16:38
 * 功能:
 */
public abstract class BaseActivity<B extends ViewBinding, P extends BasePresenter> extends RxAppCompatActivity implements BaseContract.IView {

    protected B mBinding;

    private P mPresenter;

    protected abstract P createPresenter();

    public abstract void initView();

    public abstract void initData();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mPresenter = createPresenter();
        super.onCreate(savedInstanceState);
        createViewBinding();
        if (null != mBinding) {
            setContentView(mBinding.getRoot());
        }
        if (null != mPresenter) {
            mPresenter.onAttach(this);
        }
        setStatusColor();
        initView();
        initData();
    }

    /**
     * 创建ViewBinding
     */
    private void createViewBinding() {
        try {
            Type type = getClass().getGenericSuperclass();
            Class<?> aClass = (Class<?>) ((ParameterizedType) type).getActualTypeArguments()[0];
            if (!aClass.equals(ViewBinding.class)) {
                Method method = aClass.getDeclaredMethod("inflate", LayoutInflater.class);
                mBinding = (B) method.invoke(null, getLayoutInflater());
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage());
            mBinding = null;
        }
    }

    protected void setStatusColor() {
        ImmersionBar.with(this)
                .statusBarColor(android.R.color.white)
                .statusBarDarkFont(true)
                .init();
    }

    @Override
    public AppCompatActivity getDependencies() {
        return this;
    }

    @Override
    public View getRootView() {
        return null == mBinding ? findViewById(android.R.id.content) : mBinding.getRoot();
    }

    @Override
    public <T> LifecycleTransformer<T> bindRxLifecycle() {
        return bindUntilEvent(ActivityEvent.DESTROY);
    }

    @Override
    public void alert(@NonNull String msg) {
        if (!TextUtils.isEmpty(msg)) {
            runOnUiThread(() -> ToastManager.getInstance().showToast(msg));
        }
    }

    @Override
    public void openActivity(@NonNull String link) {

    }

    @Override
    public void openActivity(@NonNull String link, @NonNull Serializable... serializables) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
