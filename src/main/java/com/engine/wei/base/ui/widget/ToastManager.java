package com.engine.wei.base.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.engine.wei.base.databinding.LayoutToastBinding;
import com.engine.wei.base.utils.Preconditions;
import com.engine.wei.base.utils.UiUtils;


/**
 * 作者:赵若位
 * 时间:2021/7/2 16:44
 * 功能:
 */
public class ToastManager {

    private static volatile ToastManager sUtils;
    private final Toast mToast;
    private LayoutToastBinding mBinding;

    private ToastManager() {
        Context context = Preconditions.checkNotNull(UiUtils.getContext(), "The Context of %s is empty!");
        mToast = new Toast(context);
        initView(context);
    }

    private void initView(Context context) {
        mBinding = LayoutToastBinding.inflate(LayoutInflater.from(context));
        mToast.setView(mBinding.getRoot());
    }

    public static ToastManager getInstance() {
        if (null == sUtils) {
            synchronized (ToastManager.class) {
                if (null == sUtils) {
                    sUtils = new ToastManager();
                }
            }
        }
        return sUtils;
    }

    public void showToast(@Nullable String msg, int delay) {
        if (!TextUtils.isEmpty(msg)) {
            mBinding.tvTitle.setText(msg);
            mToast.setDuration(delay);
            mToast.show();
        }
    }

    public void showToast(@Nullable String msg) {
        this.showToast(msg, Toast.LENGTH_SHORT);
    }


}
