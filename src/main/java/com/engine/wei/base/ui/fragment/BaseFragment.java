package com.engine.wei.base.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.engine.wei.base.mvp.contract.BaseContract;
import com.engine.wei.base.mvp.presenter.BasePresenter;
import com.engine.wei.base.ui.widget.ToastManager;
import com.engine.wei.base.utils.log.LogUtils;
import com.trello.rxlifecycle4.LifecycleTransformer;
import com.trello.rxlifecycle4.android.FragmentEvent;
import com.trello.rxlifecycle4.components.RxFragment;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 作者:赵若位
 * 时间:2021/8/25 16:38
 * 功能:
 */
public abstract class BaseFragment<B extends ViewBinding, P extends BasePresenter> extends RxFragment implements BaseContract.IView {

    private B mBinding;
    private P mPresenter;

    abstract P createPresenter();

    abstract void initView();

    abstract void initData();


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
        if (null != mPresenter) {
            mPresenter.onAttach(this);
        }
    }

    private void createViewBinding(LayoutInflater inflater, @NonNull ViewGroup container) {
        try {
            Type type = getClass().getGenericSuperclass();
            if (type instanceof ParameterizedType) {
                Class<?> aClass = (Class<?>) ((ParameterizedType) type).getActualTypeArguments()[0];
                Method method = aClass.getDeclaredMethod("inflate", LayoutInflater.class, ViewGroup.class, Boolean.class);
                mBinding = (B) method.invoke(null, inflater, container, false);
            }
        } catch (Exception e) {
            LogUtils.e(e.getMessage());
            mBinding = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        createViewBinding(inflater, container);
        initView();
        return null == mBinding ? null : mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    @Override
    public AppCompatActivity getDependencies() {
        return (AppCompatActivity) getActivity();
    }

    @Override
    public View getRootView() {
        return mBinding.getRoot();
    }

    @Override
    public <T> LifecycleTransformer<T> bindRxLifecycle() {
        return bindUntilEvent(FragmentEvent.DESTROY);
    }

    @Override
    public void alert(@NonNull String msg) {
        if (!TextUtils.isEmpty(msg)) {
            getActivity().runOnUiThread(() -> ToastManager.getInstance().showToast(msg));
        }
    }

    @Override
    public void openActivity(@NonNull String link) {

    }

    @Override
    public void openActivity(@NonNull String link, @NonNull Serializable... serializables) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
