package com.engine.wei.base.ui.imageLoader;

import android.content.Context;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.request.RequestOptions;

/**
 * 作者:赵若位
 * 时间:2020/9/28 17:23
 * 功能:
 */
public interface IImageLoader {
    /*通过Url加载图片*/
    void loadImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView);

    void loadImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView, RequestOptions options);

    /*加载圆形图片*/
    void loadCircleImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView);

    void loadCircleImage(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView, RequestOptions options);

    void loadImageByOptions(@Nullable Context context, @Nullable Object object, @Nullable ImageView imageView, RequestOptions options);
}
