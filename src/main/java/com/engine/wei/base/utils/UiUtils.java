package com.engine.wei.base.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

/**
 * 作者:赵若位
 * 时间:2021/8/26 10:22
 * 功能:
 */
public class UiUtils {
    private static final String TAG = UiUtils.class.getSimpleName();

    private static Context sContext;
    private static Handler sMainHandler = new Handler(Looper.myLooper());

    private UiUtils() {
        throw new IllegalStateException("You can't instantiate me~");
    }

    public static void init(@NonNull Context context) {
        sContext = context.getApplicationContext();
    }

    public static Context getContext() {
        return sContext;
    }

    public static Handler getHandler() {
        return sMainHandler;
    }

    /**
     * 主线程执行Runnable
     *
     * @param run
     */
    public static void post(@NonNull Runnable run) {
        if (null != sMainHandler) {
            sMainHandler.post(run);
        }
    }

    /**
     * 主线程延迟执行Runnable
     *
     * @param run
     * @param delay
     */
    public static void postDelay(@NonNull Runnable run, long delay) {
        if (null != sMainHandler) {
            sMainHandler.postDelayed(run, delay);
        }
    }

    /**
     * 主线程Handler移除Runnable
     *
     * @param run
     */
    public static void removeCallback(@NonNull Runnable run) {
        if (null != sMainHandler) {
            sMainHandler.removeCallbacks(run);
        }
    }

    /**
     * 主线程移除所有的Runnable
     */
    public static void removeAllCallback() {
        if (null != sMainHandler) {
            sMainHandler.removeCallbacksAndMessages(null);
        }
    }

}
