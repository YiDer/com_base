package com.engine.wei.base.utils;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * 作者:赵若位
 * 时间:2020/12/22 16:28
 * 功能:缓存客户端：1.网络缓存；2.硬盘缓存
 */
public class CacheManager {

    private static CacheManager sManager;
    private Context mContext;
    private String[] mCachePath;

    private CacheManager() {

    }

    public static CacheManager getInstance() {
        if (null == sManager) {
            synchronized (CacheManager.class) {
                if (null == sManager) {
                    sManager = new CacheManager();
                }
            }
        }
        return sManager;
    }

    public void init(Context context) {
        this.mContext = context.getApplicationContext();
        Preconditions.checkNotNull(mContext, "The Cache of Context is empty~");
        mCachePath = new String[]{
                mContext.getCacheDir().getAbsolutePath(),//package/cache
                mContext.getFilesDir().getAbsolutePath(),//package/file
                mContext.getDir("picture", Context.MODE_APPEND).getAbsolutePath()//图片缓存目录
        };
        checkFileExits();
    }

    /*获取文件大小*/
    private long getFileSize(File file) {
        return (null != file && file.exists()) ? file.length() : 0;
    }

    /*检验缓存文件是否存在并新建*/
    protected void checkFileExits() {
        if (null != mCachePath && mCachePath.length != 0) {
            for (String s : mCachePath) {
                File file = new File(s);
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /*获取图片缓存目录*/
    public File getPictureCacheFile() {
        File file = new File(mCachePath[2]);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    /*获取文件大小*/
    public String getCacheSizeUnit() {
        long size = getCacheSize();
        DecimalFormat format = new DecimalFormat("#.00");
        if (size < 1024) {
            return format.format(size) + "b";
        } else if (size < 1024 * 1024) {
            return format.format(size / 1024) + "kb";
        } else if (size < 1024 * 1024 * 1024) {
            return format.format(size / (1024 * 1024)) + "m";
        } else {
            return format.format(size / (1024 * 1024 * 1024)) + "G";
        }
    }

    /*获取缓存大小*/
    public long getCacheSize() {
        int size = 0;
        if (null != mCachePath && mCachePath.length != 0) {
            for (String s : mCachePath) {
                File file = new File(s);
                size += getFileSize(file);
            }
        }
        return size;
    }

    /*清除缓存*/
    public void clearCache() {
        if (null != mCachePath && mCachePath.length != 0) {
            for (String s : mCachePath) {
                File file = new File(s);
                file.delete();
            }
        }
        checkFileExits();
    }
}
