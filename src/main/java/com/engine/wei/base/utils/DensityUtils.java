package com.engine.wei.base.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * 作者:赵若位
 * 时间:2021/7/9 15:01
 * 功能:
 */
public class DensityUtils {


    public static int sp2px(Context context, int spValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue, context.getResources().getDisplayMetrics());
    }
}
