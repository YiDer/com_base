package com.engine.wei.base.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import androidx.annotation.ArrayRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

/**
 * 作者:赵若位
 * 时间:2021/8/26 16:09
 * 功能:Resources工具类
 */
public class ResourceUtils {

    private static Context sContext;
    private static Resources sResources;

    private ResourceUtils() {

    }

    public static void init(@NonNull Context context) {
        sContext = context.getApplicationContext();
    }

    public static Resources getResources() {
        if (null == sResources) {
            sResources = sContext.getResources();
        }
        return sResources;
    }


    public static String getString(@StringRes int strResId) {
        return sResources.getString(strResId);
    }

    public static String getString(@StringRes int strResId, Object... formatArgs) {
        return sResources.getString(strResId, formatArgs);
    }

    public static String[] getArrayString(@ArrayRes int strResId) {
        return sResources.getStringArray(strResId);
    }

    public static int getDimensionPixelOffset(@DimenRes int resId) {
        return sResources.getDimensionPixelOffset(resId);
    }

    public static int getDimension(@DimenRes int resId) {
        return (int) sResources.getDimension(resId);
    }

    public static int getColor(@ColorRes int colorResId) {
        return sResources.getColor(colorResId);
    }

    public static Drawable getDrawable(@DrawableRes int drResId) {
        return getResources().getDrawable(drResId);
    }

    public static Drawable getDrawable(String name, String defType) {
        int resourceId = getResources().getIdentifier(name, defType, sContext.getPackageName());
        if (0 == resourceId) {
            return null;
        }
        return getResources().getDrawable(resourceId);
    }
}
