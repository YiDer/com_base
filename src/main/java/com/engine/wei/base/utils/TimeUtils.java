package com.engine.wei.base.utils;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/*
 * 作者:赵若位
 * 时间:2021/2/15 18:29
 * 功能:
 */
public class TimeUtils {


    public static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_DATE_TIME_FORMAT_1 = "yyyy/MM/dd";
    public static final String DEFAULT_DATE_TIME_FORMAT_2 = "yyyy-MM-dd HH:mm";
    public static final String DEFAULT_DATE_TIME_FORMAT_3 = "HH:mm";
    public static final String DEFAULT_DATE_TIME_FORMAT_4 = "yyyy-MM-dd";
    public static final String DEFAULT_DATE_TIME_FORMAT_5 = "yyyy年MM月dd日";
    private static volatile ThreadLocal<SimpleDateFormat> sDateFormatThreadLocal;

    /*时间选择器截至日期*/
    public static Calendar getDeadLineCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2025, 1, 1);
        return calendar;
    }

    public static long getYearMothDayHorNow() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
        try {
            return format.parse(format.format(new Date(System.currentTimeMillis()))).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String formatTime(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH时");
        return format.format(new Date(time));
    }

    public static String getBrithday(Date date) {
        return formatTime(date.getTime(), "yyyy-MM-dd");
    }

    public static String formatTime(long time, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = new Date(time);
        return format.format(date);
    }

    public static String getCurDateTime() {
        return getCurDateTime(DEFAULT_DATE_TIME_FORMAT);
    }

    public static String getCurDateTime(String format) {
        SimpleDateFormat dateFormat = getDateFormat();
        dateFormat.applyPattern(format);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getDateBySecond(long seconds) {
        return getDateByMillis(seconds * 1000);
    }

    public static String getDateByMillis(long millisTime) {
        return getDateTime(millisTime, DEFAULT_DATE_TIME_FORMAT);
    }

    public static String getDateBySecond(long seconds, String format) {
        return getDateByMillis(seconds * 1000, format);
    }

    public static String getDateByMillis(long millisTime, String format) {
        return getDateTime(millisTime, format);
    }

    public static String getDateTime(long millisTime, String format) {
        SimpleDateFormat dateFormat = getDateFormat();
        dateFormat.applyPattern(format);
        return dateFormat.format(new Date(millisTime));
    }

    public static String getDateTime(long millisTime, String format, TimeZone timeZone) {
        SimpleDateFormat dateFormat = getDateFormat();
        TimeZone originTimeZone = dateFormat.getTimeZone();

        dateFormat.setTimeZone(timeZone);
        dateFormat.applyPattern(format);
        String result = dateFormat.format(new Date(millisTime));

        dateFormat.setTimeZone(originTimeZone);

        return result;
    }

    public static TimeZone createGmtTimeZone() {
        return TimeZone.getTimeZone("GMT");
    }

    public static TimeZone createTimeZone(boolean isGmt) {
        return isGmt ? createGmtTimeZone() : TimeZone.getDefault();
    }


    public static String getYear() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        // 时间戳转换日期
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    /**
     * 时间戳转换日期
     */
    public static String stampToTime(String stamp) {
        String sd = "";
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 时间戳转换日期
        sd = sdf.format(new Date(Long.parseLong(stamp)));
        return sd;
    }

    /**
     * 计算time2减去time1的差值 差值只设置 几天 几个小时 或 几分钟
     * 根据差值返回多长之间前或多长时间后
     */
    public static long getDistanceTime(long time1, long time2) {
        if (time1 < time2) {
            return time2 - time1;
        } else {
            return time1 - time2;
        }
    }

    /**
     * 计算time2减去time1的差值 差值只设置 几天 几个小时 或 几分钟
     * 根据差值返回多长之间前或多长时间后
     */
    public static String getDistanceTimeStr(long time1, long time2) {
        long diff;
        if (time1 < time2) {
            diff = time2 - time1;
        } else {
            diff = time1 - time2;
        }

        return getDistanceTimeStr(diff);
    }

    /**
     * 返回文本形式
     *
     * @param diff
     * @return
     */
    public static String getDistanceTimeStr(long diff) {
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;

        day = diff / (24 * 60 * 60 * 1000);
        hour = (diff / (60 * 60 * 1000) - day * 24);
        min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
        sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        if (day != 0) {
            return day + "天" + hour + "小时" + min + "分钟" + sec + "秒";
        }
        if (hour != 0) {
            return hour + "小时" + min + "分钟" + sec + "秒";
        }
        if (min != 0) {
            return min + "分钟" + sec + "秒";
        }
        if (sec != 0) {
            return sec + "秒";
        }
        return "0秒";
    }

    /**
     * 返回文本形式
     *
     * @param diff
     * @return
     */
    public static String getTimeStrForTime(long diff) {
        long hour = 0;
        long min = 0;
        long sec = 0;
        String tempHour;
        String tempMin;
        String tempSec;

        hour = (diff / (60 * 60 * 1000));
        min = ((diff / (60 * 1000)) - hour * 60);
        sec = (diff / 1000 - hour * 60 * 60 - min * 60);
        if (hour < 10) {
            tempHour = "0" + hour;
        } else {
            tempHour = "" + hour;
        }
        if (min < 10) {
            tempMin = "0" + min;
        } else {
            tempMin = "" + min;
        }
        if (sec < 10) {
            tempSec = "0" + sec;
        } else {
            tempSec = "" + sec;
        }
        if (hour != 0) {
            return tempHour + ":" + tempMin + ":" + tempSec;
        }
        if (min != 0) {
            return "00:" + tempMin + ":" + tempSec;
        }
        if (sec != 0) {
            return "00:00:" + tempSec + "";
        }
        return "0";
    }

    /**
     * 当天时间为 00:00 的时间戳
     *
     * @return
     */
    public static long getMillisOnZeroToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    @NonNull
    private static SimpleDateFormat getDateFormat() {
        if (sDateFormatThreadLocal == null) {
            synchronized (TimeUtils.class) {
                if (sDateFormatThreadLocal == null) {
                    sDateFormatThreadLocal = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT,
                                    Locale.getDefault());
                        }
                    };
                }
            }
        }

        return sDateFormatThreadLocal.get();
    }

    public static long getDateTime(int year, int month, int day, int hour, int minute,
                                   int second, int millis) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, millis);

        return cal.getTimeInMillis();
    }
}
