package com.engine.wei.base.utils.log;


import androidx.annotation.NonNull;

import com.alibaba.android.arouter.facade.template.ILogger;
import com.engine.wei.base.BuildConfig;
import com.tencent.mars.xlog.Log;

/**
 * 作者:赵若位
 * 时间:2021/8/25 17:00
 * 功能:
 */
public class LogUtils implements ILogger {

    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = BuildConfig.TAG;


    private static String getLogPathName() {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[4];
        String className = stackTraceElement.getClassName();
        int index = className.lastIndexOf('.');
        if (index != -1) {
            className = className.substring(index + 1);
        }
        return className + "." + stackTraceElement.getMethodName() + "(" + stackTraceElement.getLineNumber() + ")" + ": ";
    }

    public static void d(@NonNull String msg) {
        if (DEBUG) {
            Log.d(getLogPathName(), msg);
        }
    }

    public static void d(@NonNull String msg, final Object... obj) {
        if (DEBUG) {
            Log.d(getLogPathName(), msg, obj);
        }
    }

    public static void i(@NonNull String msg) {
        if (DEBUG) {
            Log.i(getLogPathName(), msg);
        }
    }

    public static void i(@NonNull String msg, final Object... obj) {
        if (DEBUG) {
            Log.i(getLogPathName(), msg, obj);
        }
    }

    public static void w(@NonNull String msg) {
        if (DEBUG) {
            Log.w(getLogPathName(), msg);
        }
    }

    public static void w(@NonNull String msg, final Object... obj) {
        if (DEBUG) {
            Log.w(getLogPathName(), msg, obj);
        }
    }

    public static void e(@NonNull String msg) {
        if (DEBUG) {
            Log.e(getLogPathName(), msg);
        }
    }

    public static void e(@NonNull String msg, final Object... obj) {
        if (DEBUG) {
            Log.e(getLogPathName(), msg, obj);
        }
    }

    @Override
    public void showLog(boolean isShowLog) {

    }

    @Override
    public void showStackTrace(boolean isShowStackTrace) {

    }

    @Override
    public void debug(String tag, String message) {
        d(message);
    }

    @Override
    public void info(String tag, String message) {
        i(message);
    }

    @Override
    public void warning(String tag, String message) {
        w(message);
    }

    @Override
    public void error(String tag, String message) {
        e(message);
    }

    @Override
    public void monitor(String message) {
        e("monitor:" + message);
    }

    @Override
    public boolean isMonitorMode() {
        return true;
    }

    @Override
    public String getDefaultTag() {
        return getLogPathName();
    }
}
