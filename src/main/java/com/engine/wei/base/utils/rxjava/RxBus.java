package com.engine.wei.base.utils.rxjava;


import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * 作者:赵若位
 * 时间:2021/7/1 17:19
 * 功能:
 */
public class RxBus {

    private final Subject<Object> mSubject;

    private RxBus() {
        mSubject = PublishSubject.create().toSerialized();
    }

    public final static RxBus getInstance() {
        return Holder.BUS;
    }

    public void post(Object o) {
        mSubject.onNext(o);
    }

    public <T> Observable<T> toObservable(Class<T> cls) {
        return mSubject.ofType(cls);
    }

    public Observable<Object> toObservable() {
        return mSubject.hide();
    }

    public boolean hasObservable() {
        return mSubject.hasObservers();
    }

    private static class Holder {
        private static final RxBus BUS = new RxBus();
    }
}
