package com.engine.wei.base.utils.aroute;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.SerializationService;
import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * 作者:赵若位
 * 时间:2021/3/18 20:57
 * 功能:
 */
@Route(path = "service/json")
public class SerializationServiceImpl implements SerializationService {

    private Gson mGson;

    @Override
    public <T> T json2Object(String input, Class<T> clazz) {
        if (!TextUtils.isEmpty(input)) {
            return mGson.fromJson(input, clazz);
        }
        return null;
    }

    @Override
    public String object2Json(Object instance) {
        if (null != instance) {
            return mGson.toJson(instance);
        }
        return null;
    }

    @Override
    public <T> T parseObject(String input, Type clazz) {
        if (!TextUtils.isEmpty(input)) {
            return mGson.fromJson(input, clazz);
        }
        return null;
    }

    @Override
    public void init(Context context) {
        mGson = new Gson();
    }
}
