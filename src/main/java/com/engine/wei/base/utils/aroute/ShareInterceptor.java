package com.engine.wei.base.utils.aroute;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;

/**
 * 作者:赵若位
 * 时间:2021/7/22 22:13
 * 功能:ARoute拦截器
 */
@Interceptor(priority = 1)
public class ShareInterceptor implements IInterceptor {

    private Context mContext;

    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {
//        switch (postcard.getPath()) {
//            case LinkPath.LINK_NULLNONE_PATH:
//                if (null != postcard && null != postcard.getExtras()) {
//                    try {
//                        String url = (String) postcard.getExtras().getSerializable("web");
//                        if (LinkPath.LINK_QQ_PATH.equals(url)) {
//                            SystemUtils.openQQ(mContext);
//                            callback.onInterrupt(null);
//                        }
//                        if (LinkPath.LINK_WEIXIN_PATH.equals(url)) {
//                            SystemUtils.openWeixin(mContext);
//                            callback.onInterrupt(null);
//                        }
//                    } catch (Exception e) {
//                        L.e(e.getMessage());
//                    } finally {
//                        callback.onInterrupt(null);
//                    }
//                }
//                callback.onInterrupt(null);
//                break;
//            default:
//                callback.onContinue(postcard);
//                break;
//        }
    }

    @Override
    public void init(Context context) {
        this.mContext = context;
    }
}
