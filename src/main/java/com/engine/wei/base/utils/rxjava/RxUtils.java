package com.engine.wei.base.utils.rxjava;


import android.content.Context;

import androidx.annotation.NonNull;

import com.engine.wei.base.mvp.contract.BaseContract;
import com.engine.wei.base.utils.log.LogUtils;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.ObservableTransformer;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.core.SingleTransformer;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * 作者:赵若位
 * 时间:2021/8/25 18:04
 * 功能:
 */
public class RxUtils {

    private RxUtils() {
        throw new IllegalStateException("You can't instantiate me~");
    }

    /**
     * Observable添加进度条
     *
     * @param view
     * @param <T>
     * @return
     */
    public static <T> ObservableTransformer<T, T> observableLoading(@NonNull BaseContract.IView view) {
        return null != view ? observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (null != view) {
                        LogUtils.i("%s 开始执行进度条", view.getClass().getName());
                        view.showLoading();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {
                    LogUtils.i("%s 开始关闭进度条", view.getClass().getName());
                    if (null != view) {
                        view.hideLoading();
                    }
                }).unsubscribeOn(Schedulers.io())
                .compose(bindRxLifecycle(view)) : observable -> observable;
    }

    /**
     * Single 添加进度条
     *
     * @param view
     * @param <T>
     * @return
     */
    public static <T> SingleTransformer<T, T> singleLoading(@NonNull BaseContract.IView view) {
        return null != view ? new SingleTransformer<T, T>() {
            @Override
            public @io.reactivex.rxjava3.annotations.NonNull SingleSource<T> apply(@io.reactivex.rxjava3.annotations.NonNull Single<T> single) {
                return single.observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> {
                            if (null != view) {
                                view.showLoading();
                            }
                        }).observeOn(AndroidSchedulers.mainThread())
                        .doFinally(() -> {
                            if (null != view) {
                                view.hideLoading();
                            }
                        });
            }
        } : single -> single;
    }

    public static <T> ObservableTransformer<T, T> bindRxLifecycle(@NonNull Context context) {
        return null != context && context instanceof BaseContract.IView ?
                ((BaseContract.IView) context).bindRxLifecycle() : upstream -> upstream;
    }

    public static <T> ObservableTransformer<T, T> bindRxLifecycle(@NonNull BaseContract.IView view) {
        return null != view ? view.bindRxLifecycle() : upstream -> upstream;
    }

    /*RxJava线程切换*/
    public static <T> ObservableTransformer<T, T> schedulers() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io());
    }

}
