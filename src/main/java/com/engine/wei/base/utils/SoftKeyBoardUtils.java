package com.engine.wei.base.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import com.engine.wei.base.utils.log.LogUtils;

/**
 * 作者:赵若位
 * 时间:2021/8/9 17:45
 * 功能:软键盘相关操作类
 */
public class SoftKeyBoardUtils {

    /**
     * 显示软键盘
     *
     * @param view
     */
    public static void showSoftKeyBoard(@Nullable View view) {
        if (null != view) {
            LogUtils.i("showSoftKeyBoard.Context:" + view.getContext().getClass().getName());
            if (view.getContext() instanceof LifecycleOwner) {
                ((LifecycleOwner) view.getContext()).getLifecycle()
                        .addObserver(new LifecycleObserver() {
                            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
                            public void onHideSoftKeyBoard(LifecycleOwner owner) {
                                LogUtils.i(getClass().getName() + "showSoftKeyBoard=>onHideSoftKeyBoard");
                                hideSoftKeyBoard(view);
                                if (null != owner) {
                                    owner.getLifecycle().removeObserver(this);
                                }
                            }
                        });
            }
            final InputMethodManager manager = (InputMethodManager) view.getContext()
                    .getApplicationContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (null != manager) {
                view.requestFocus();
                LogUtils.i("showSoftKeyBoard");
                manager.showSoftInput(view, 0);
            }
        }
    }

    /**
     * 隐藏软键盘
     *
     * @param view
     */
    public static void hideSoftKeyBoard(@Nullable View view) {
        if (null != view) {
            final InputMethodManager manager = (InputMethodManager) view.getContext()
                    .getApplicationContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (null != manager) {
                manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                LogUtils.i("hideSoftKeyBoard");
            }
        }
    }

    /**
     * 切换软键盘，例如如果软键盘打开->关闭；如果关闭->打开
     *
     * @param view
     */
    public static void toggleSoftKeyBoard(@Nullable View view) {
        if (null != view) {
//            if (view.getContext() instanceof LifecycleOwner) {
//                ((LifecycleOwner) view.getContext()).getLifecycle()
//                        .addObserver(new LifecycleObserver() {
//                            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
//                            public void onHideSoftKeyBoard(LifecycleOwner owner) {
//                                L.e(getClass().getName() + "=>开始隐藏弹出的软键盘");
//                                hideSoftKeyBoard(view);
//                                if (null != owner) {
//                                    owner.getLifecycle().removeObserver(this);
//                                }
//                            }
//                        });
//            }
            final InputMethodManager manager = (InputMethodManager) view.getContext()
                    .getApplicationContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (null != manager) {
                manager.toggleSoftInputFromWindow(view.getWindowToken(), 0, 0);
                LogUtils.i("toggleSoftKeyBoard");
            }
        }
    }
}
