package com.engine.wei.base.utils;

import androidx.annotation.Nullable;

/**
 * 作者:赵若位
 * 时间:2020/8/20 10:46
 * 功能:
 */
public final class Preconditions {
    private Preconditions() {
        throw new AssertionError("No instances.");
    }

    /**
     * 检测对象是否为空
     *
     * @param value
     * @param errorMessge
     * @param <T>
     * @return
     */
    public static <T> T checkNotNull(T value, @Nullable Object errorMessge) {
        if (null == value) {
            throw new NullPointerException(String.valueOf(errorMessge));
        } else {
            return value;
        }
    }

    /**
     * 检测对象是否为空
     *
     * @param value
     * @param errorMessageTemplate
     * @param errorMessage
     * @param <T>
     * @return
     */
    public static <T> T checkNotNull(T value, @Nullable String errorMessageTemplate, @Nullable Object... errorMessage) {
        if (null == value) {
            throw new NullPointerException(String.format(errorMessageTemplate, errorMessage));
        } else {
            return value;
        }
    }
}
