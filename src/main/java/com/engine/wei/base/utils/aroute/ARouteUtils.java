package com.engine.wei.base.utils.aroute;

import android.app.Activity;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.launcher.ARouter;
import com.engine.wei.base.R;

import java.io.Serializable;

/**
 * 作者:赵若位
 * 时间:2021/3/15 23:37
 * 功能:ARoute路由跳转框架
 */
public class ARouteUtils {

    private static Postcard getBuild(@Nullable String path) {
        if (!TextUtils.isEmpty(path)) {
            return ARouter.getInstance().build(path)
                    .withTransition(R.anim.anima_left_in, R.anim.anima_right_out);
        }
        return null;
    }

    /*不带参数跳转*/
    public static void navigation(@Nullable String path) {
        Postcard post = getBuild(path);
        if (null != post) {
            post.navigation();
        }
    }

    public static void navigation(@Nullable String path, @NonNull String... parameterString) {
        Postcard post = getBuild(path);
        if (null != post) {
            if (null != parameterString && parameterString.length != 0) {
                for (int i = 0; i < parameterString.length; i = i + 2) {
                    post.withString(parameterString[i], parameterString[i + 1]);
                }
            }
            post.navigation();
        }
    }

    public static void navigation(@Nullable String path, @Nullable String key, Object object) {
        Postcard post = getBuild(path);
        if (null != post && !TextUtils.isEmpty(key)) {
            post.withObject(key, object);
            post.navigation();
        }
    }


    public static void navigation(@Nullable String path, @Nullable String key, Serializable object) {
        Postcard post = getBuild(path);
        if (null != post && !TextUtils.isEmpty(key)) {
            post.withSerializable(key, object);
            post.navigation();
        }
    }

    public static void navigation(@Nullable String path, @Nullable String key, int object) {
        Postcard post = getBuild(path);
        if (null != post && !TextUtils.isEmpty(key)) {
            post.withInt(key, object);
            post.navigation();
        }
    }

    //开始执行PostCard

    public static void navigation(@NonNull Activity activity, @Nullable String path, int result) {
        Postcard post = getBuild(path);
        if (null != post) {
            post.navigation(activity, result);
        }
    }
}
