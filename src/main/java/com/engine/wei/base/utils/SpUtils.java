package com.engine.wei.base.utils;

import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.tencent.mmkv.MMKV;

/**
 * 作者:赵若位
 * 时间:2021/7/8 17:46
 * 功能:基于MVVM的xml存储
 */
public class SpUtils {

    private static volatile SpUtils sUtils;
    private static MMKV sMMKV;

    private SpUtils() {
        sMMKV = MMKV.defaultMMKV();
    }

    public static SpUtils getUtils() {
        if (null == sUtils) {
            synchronized (SpUtils.class) {
                if (null == sUtils) {
                    sUtils = new SpUtils();
                }
            }
        }
        return sUtils;
    }

    public boolean put(@Nullable String key, @Nullable Object obj) {
        if (TextUtils.isEmpty(key) || null == obj) {
            return false;
        }
        if (obj instanceof Integer) {
            return sMMKV.encode(key, (Integer) obj);
        }

        if (obj instanceof Boolean) {
            return sMMKV.encode(key, (Boolean) obj);
        }

        if (obj instanceof Long) {
            return sMMKV.encode(key, (Long) obj);
        }

        if (obj instanceof Float) {
            return sMMKV.encode(key, (Float) obj);
        }

        if (obj instanceof String) {
            return sMMKV.encode(key, (String) obj);
        }

        if (obj instanceof Double) {
            return sMMKV.encode(key, (Double) obj);
        }

        if (obj instanceof Byte[]) {
            return sMMKV.encode(key, (byte[]) obj);
        }

        if (obj instanceof Parcelable) {
            return sMMKV.encode(key, (Parcelable) obj);
        }
        return false;
    }

    public int getInteger(@Nullable String key, int defaultInteger) {
        return TextUtils.isEmpty(key) ? 0 : sMMKV.decodeInt(key, defaultInteger);
    }

    public Long getLong(@Nullable String key, Long defaultLong) {
        return TextUtils.isEmpty(key) ? null : sMMKV.decodeLong(key, defaultLong);
    }

    public String getString(@Nullable String key, String defaultString) {
        return TextUtils.isEmpty(key) ? null : sMMKV.decodeString(key, defaultString);
    }

    public boolean getBoolean(@Nullable String key, Boolean defaultBoolean) {
        return TextUtils.isEmpty(key) ? null : sMMKV.decodeBool(key, defaultBoolean);
    }

    public double getDouble(@Nullable String key, double defaultDouble) {
        return TextUtils.isEmpty(key) ? 0 : sMMKV.decodeDouble(key, defaultDouble);
    }

    public float getFloat(@Nullable String key, float defaultFloat) {
        return TextUtils.isEmpty(key) ? 0 : sMMKV.decodeFloat(key, defaultFloat);
    }

    public byte[] getFloat(@Nullable String key, byte[] defaultByteArray) {
        return TextUtils.isEmpty(key) ? null : sMMKV.decodeBytes(key, defaultByteArray);
    }

    public <T extends Parcelable> T getParcelable(@Nullable String key, Class<T> cls, T defaultParcelable) {
        return TextUtils.isEmpty(key) ? null : sMMKV.decodeParcelable(key, cls, defaultParcelable);
    }

    public void removeKey(@Nullable String key) {
        if (null != sMMKV && !TextUtils.isEmpty(key)) {
            sMMKV.removeValueForKey(key);
        }
    }

    public void clearAll() {
        if (null != sMMKV) {
            sMMKV.clearAll();
        }
    }
}
