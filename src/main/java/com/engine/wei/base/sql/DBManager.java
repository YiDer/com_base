package com.engine.wei.base.sql;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * 作者:赵若位
 * 时间:2020/12/22 16:21
 * 功能:数据库客户端
 */
//@Database(entities = {}, version = 1)
public abstract class DBManager extends RoomDatabase {

    private static volatile DBManager sManager = null;

    public static DBManager getInstance(@Nullable Context context) {
        if (null == sManager) {
            synchronized (DBManager.class) {
                if (null == sManager) {
                    sManager = Room.databaseBuilder(context,
                            DBManager.class, context.getPackageName() + ".db")
//                            .addMigrations()
//                            .allowMainThreadQueries()//是否允许在主线程操作数据库
//                            .createFromAsset("database/campus.db")//从assets文件夹中预装载数据库
                            .fallbackToDestructiveMigration()//数据库升级的时候不会崩溃，但是数据会重置,数据会清空
                            .build();
                }
            }
        }
        return sManager;
    }


//    public abstract LoginTokenDao providerTokenDao();
//
//    public abstract SchoolDao providerSchoolDao();
}
