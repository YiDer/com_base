package com.engine.wei.base.mvp.contract;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.trello.rxlifecycle4.LifecycleTransformer;

import java.io.Serializable;

/**
 * 作者:赵若位
 * 时间:2021/8/25 17:43
 * 功能:
 */
public interface BaseContract {

    interface IView {
        AppCompatActivity getDependencies();

        View getRootView();

        <T> LifecycleTransformer<T> bindRxLifecycle();

        void alert(@NonNull String msg);

        void openActivity(@NonNull String link);

        void openActivity(@NonNull String link, @NonNull Serializable... serializables);

        void showLoading();

        void hideLoading();
    }

    interface IModel {
        void onCreate();

        void onDestroy();
    }
}
