package com.engine.wei.base.mvp.model;

import com.engine.wei.base.mvp.contract.BaseContract;

/**
 * 作者:赵若位
 * 时间:2021/8/25 17:51
 * 功能:
 */
public class BaseModel implements BaseContract.IModel {
    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {

    }
}
