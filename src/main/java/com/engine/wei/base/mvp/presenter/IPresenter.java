package com.engine.wei.base.mvp.presenter;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import com.engine.wei.base.mvp.contract.BaseContract;

/**
 * 作者:赵若位
 * 时间:2021/8/25 17:49
 * 功能:
 */
public interface IPresenter<V extends BaseContract.IView> extends LifecycleObserver {

    void onAttach(@NonNull V view);

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void onDestroy(LifecycleOwner owner);
}
