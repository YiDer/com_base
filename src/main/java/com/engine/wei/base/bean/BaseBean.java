package com.engine.wei.base.bean;

/**
 * 作者:赵若位
 * 时间:2021/8/25 16:30
 * 功能:基础的Java实体对象
 */
public class BaseBean<T> {

    private int code;
    private String error;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
